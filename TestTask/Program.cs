﻿using System;

namespace TestTask
{
    class Program
    {
        static void Main(string[] args)
        {
            bool continueFlag = true;
           
            while (continueFlag)
            {
                bool checkFlag = true;
                Console.WriteLine("Введите массив 3x3:");
                int[,] inputMassive = new int[3, 3];
                for (int i = 0; i < inputMassive.GetLength(0); i++)
                {
                    string enterString = Console.ReadLine();
                    string[] massiveString = enterString.Split(new Char[] { ' ' });
                    if (massiveString.Length != 3)
                    {
                        Console.WriteLine("Размер массива 3x3. Неверное количество элементов!");
                        checkFlag = false;
                        break;
                    }

                    for (int j = 0; j < massiveString.Length; j++)
                    {
                        if (int.Parse(massiveString[j]) > 0 && int.Parse(massiveString[j]) < 10)
                        {
                            inputMassive[i, j] = int.Parse(massiveString[j]);
                        }
                        else
                        {
                            Console.WriteLine("Элементы должны быть от 1 до 9");
                            checkFlag = false;
                            break;
                        }
                    }

                    if (!checkFlag)
                    {
                        break;
                    }
                }
                if (!checkFlag)
                {
                    continue;
                }

                string path = "";
                Path(ref inputMassive, ref path);
                Console.WriteLine($"Путь, начиная с последнего элемента в последней строке:\n{path}");

                Console.WriteLine("Продолжить? Y/N");
                string answer = Console.ReadLine();
                if (answer == "N" || answer == "n")
                {
                    continueFlag = false;
                }
            }
        }

        //Метод обходит массив, начиная с последнего элемента в последней строке. Каждый раз, прежде чем шагать происходит сравнение соседних элементов. 
        //Если элемент максимальный, то шаг происходит на него. Если мы сейчас находимся на центральном элементе, то, если есть непройденный элемент рядом с начальным,
        //то шагаем на него. Посещенные ячейки заполняются нулями.
        static void Path(ref int[,] mass, ref string path)
        {
            int i = 2, j = 2;

            while (mass[i, j] != 0)
            {
                path += mass[i, j];
                mass[i, j] = 0;

                if (i == 1 && j == 1)
                {
                    if (!IsZeroValue(mass[i + 1, j]))
                    {
                        i++;
                        continue;
                    }

                    if (!IsZeroValue(mass[i, j + 1]))
                    {
                        j++;
                        continue;
                    }
                }

                if (i == 2)
                {
                    if (j == 2)
                    {
                        Compare(mass[i, j - 1], 0, 0, mass[i - 1, j], ref i, ref j);
                        continue;
                    }
                    else
                    {
                        if (j == 0)
                        {
                            Compare(0, mass[i, j + 1], 0, mass[i - 1, j], ref i, ref j);
                            continue;
                        }

                        if (j == 1)
                        {
                            Compare(mass[i, j - 1], mass[i, j + 1], 0, mass[i - 1, j], ref i, ref j);
                            continue;
                        }
                    }
                }
                else if (i == 0)
                {
                    if (j == 2)
                    {
                        Compare(mass[i, j - 1], 0, mass[i + 1, j], 0, ref i, ref j);
                        continue;
                    }
                    else
                    {
                        if (j == 0)
                        {
                            Compare(0, mass[i, j + 1], mass[i + 1, j], 0, ref i, ref j);
                            continue;
                        }

                        if (j == 1)
                        {
                            Compare(mass[i, j - 1], mass[i, j + 1], mass[i + 1, j], 0, ref i, ref j);
                            continue;
                        }
                    }
                }
                else if (i == 1)
                {
                    if (j == 2)
                    {
                        Compare(mass[i, j - 1], 0, mass[i + 1, j], mass[i - 1, j], ref i, ref j);
                        continue;
                    }
                    else
                    {
                        if (j == 0)
                        {
                            Compare(0, mass[i, j + 1], mass[i + 1, j], mass[i - 1, j], ref i, ref j);
                            continue;
                        }

                        if (j == 1)
                        {
                            Compare(mass[i, j - 1], mass[i, j + 1], mass[i + 1, j], mass[i - 1, j], ref i, ref j);
                            continue;
                        }
                    }
                }
            }
        }

        static bool IsZeroValue(int num)
        {
            if (num == 0)
            {
                return true;
            }
            return false;
        }

        static void Compare(int left, int right, int low, int high, ref int i, ref int j)
        {
            if (left != 0 || right != 0 || low != 0 || high != 0)
            {
                if (left > right && left > low && left > high) { j--; }
                else if (right > low && right > high) { j++; }
                else if (low > high) { i++; }
                else i--;
            }
        }
    }
}
